export default function ajax(url, requestData) {
  if (!requestData) {
    requestData = {}
  }
  url = `http://localhost:3333${url}`
  const options = {
    method: 'POST',
    body: JSON.stringify(requestData),
    headers: {
      'Content-Type': 'application/json'
    }
  }
  return new Promise((resolve, reject) => {
    fetch(url, options).then(responseData => {
      return responseData.json()
    }).then(responseJson => {
      resolve(responseJson)
    }).catch(error => {
      reject(error)
    })
  })
}
