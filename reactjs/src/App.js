import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import BusAdd from './routes/bus/BusAdd'
import BusEdit from './routes/bus/BusEdit'
import Home from './routes/Home'

function App() {
  return <>
    <Router>
      <Routes>
        <Route path='/' element={<Home />}></Route>
        <Route path='/bus/edit/:busId' element={<BusEdit />}></Route>
        <Route path='/bus/add' element={<BusAdd />}></Route>
      </Routes>
    </Router>
  </>
}

export default App
