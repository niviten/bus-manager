import { useParams } from 'react-router-dom'

function BusEdit() {
  const { busId } = useParams()
  const title = busId ? `Edit bus ${busId}` : 'Add bus'
  return <>
    <h3>{ title }</h3>
    <hr />
  </>
}

export default BusEdit
