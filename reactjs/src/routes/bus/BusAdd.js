import { useState } from 'react'
import ajax from '../../helpers/ajax'

function BusAdd() {
  const [ busRouteID, setBusRouteID ] = useState('')
  const [ busName, setBusName ] = useState('')
  const [ message, setMessage ] = useState('')

  function onChangeBusRouteID(event) {
    setBusRouteID(event.target.value)
  }

  function onChangeBusName(event) {
    setBusName(event.target.value)
  }

  function addBus() {
    setMessage('')
    if (!busRouteID) {
      setMessage('Bus Route ID cannot be empty')
      return
    }
    const url = '/api/json/bus/add'
    const busData = { busRouteID, busName }
    ajax(url, busData).then(responseData => {
      if (responseData.status === 'SUCCESS') {
        alert(responseData.message)
        // need to navigate to the page where route details of this bus has to be added
        return
      }
    })
  }

  return <>
    <h3>Add Bus</h3>
    <hr />
    Bus Route ID: <input type="text" onChange={event => onChangeBusRouteID(event)}/> <br /> <br />
    Bus Name: <input type="text" onChange={event => onChangeBusName(event)}/> <br /> <br />
    <button onClick={() => addBus()}>Add Bus</button> <br /> <br />
    <p>{ message }</p>
  </>
}

export default BusAdd
