import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import ajax from './../helpers/ajax'

function Home() {
  const navigate = useNavigate()
  const [ buses, setBuses ] = useState([])

  useEffect(() => {
    const url = '/api/json/bus/get'
    ajax(url).then(responseData => {
      if (responseData.status === 'SUCCESS') {
        setBuses(responseData.buses)
      }
    })
  })

  function addBus() {
    navigate('/bus/add')
  }

  function onSelectBus(busId) {
    navigate(`/bus/edit/${busId}`)
  }

  return <>
    <h3>Buses</h3>
    <hr />
    <button onClick={() => addBus()}>Add Bus</button>
    <hr />
    <table className="table table-bordered table-hover">
      <thead className="table-dark">
        <tr>
          <th>BUS ROUTE ID</th>
          <th>BUS NAME</th>
        </tr>
      </thead>
      <tbody>
          {
            buses.map(bus => {
              return <tr key={bus.busID} onClick={() => onSelectBus(bus.busID)}>
                <td>{ bus.busRouteID }</td>
                <td>{ bus.busName }</td>
              </tr>
            })
          }
      </tbody>
    </table>
  </>
}

export default Home
