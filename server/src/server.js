const express = require('express')
const app = express()
const port = 3333
const cors = require('cors')

const { resolve } = require('path')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors({
  origin: 'http://localhost:3000',
  methods: ['POST', 'GET'],
  credentials: true
}))

app.all('/test', (req, res) => {
  const { name, age } = req.body
  res.send({ message: `hello there, your name is ${name}, your age is ${age}` })
})

const busAPI = require(resolve(__dirname, 'api', 'bus.js'))
app.all('/api/json/bus/add', busAPI.addBus)
app.all('/api/json/bus/get', busAPI.getBus)

app.listen(port, () => {
  console.log(`server started @ ${port}`)
})
