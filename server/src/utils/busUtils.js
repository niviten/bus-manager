const { resolve } = require('path')
const { executeQuery } = require(resolve(__dirname, '..', 'helpers', 'database.js'))

function isBusIDNew(busId) {
  const query = 'select count(BUS_ID) as BUS_COUNT from Bus where BUS_ROUTE_ID = ?'
  return new Promise((resolve, reject) => {
    executeQuery(query, [busId]).then(rows => {
      if (rows[0].BUS_COUNT === 0) {
        resolve()
      } else {
        reject()
      }
    })
  })
}

module.exports = { isBusIDNew }
