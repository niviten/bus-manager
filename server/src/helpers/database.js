const sqlite3 = require('sqlite3').verbose()
const { resolve } = require('path')
const datastoreLocation = resolve(__dirname, '..', '..', 'database', 'datastore.db')
const db = new sqlite3.Database(datastoreLocation)

function executeQuery(...args) {
    return new Promise((resolve, reject) => {
        db.all(...args, (err, rows) => {
            if (err) {
                reject(err)
                return
            }
            resolve(rows)
        })
    })
}

module.exports = { executeQuery }
