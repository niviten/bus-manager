const { executeQuery } = require("../helpers/database")
const { isBusIDNew } = require("../utils/busUtils")

function addBus(req, res) {
  let { busRouteID, busName } = req.body
  if (!busRouteID) {
    res.send({
      message: 'Invalid Bus ID',
      status: 'FAIL'
    })
    return
  }
  if (!busName) {
    busName = busRouteID
  }
  isBusIDNew(busRouteID).then(() => {
    const query = 'INSERT INTO Bus (BUS_ROUTE_ID, BUS_NAME) VALUES (?, ?)'
    const params = [ busRouteID, busName ]
    executeQuery(query, params).then(() => {
      res.send({
        message: 'Bus successfully added',
        status: 'SUCCESS'
      })
    }).catch((error) => {
      res.send({
        message: `Error while adding bus: ${error}`,
        status: 'FAIL'
      })
    })
  }).catch(() => {
    res.send({
      message: 'Bus ID already present',
      status: 'FAIL'
    })
  })
}

function getBus(req, res) {
  const query = 'SELECT BUS_ID AS busID, BUS_ROUTE_ID AS busRouteID, BUS_NAME as busName from Bus'
  executeQuery(query).then(rows => {
    res.send({
      status: 'SUCCESS',
      buses: rows
    })
  }).catch(error => {
    res.send({
      status: 'FAIL',
      message: `Error while getting buses: ${error}`
    })
  })
}

module.exports = { addBus, getBus }
